#!/bin/bash
mkdir -p ~/projects/stam/unit-test
code ~/projects/stam/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

# git remote add origin https://gitlab.com/kovanka/unit-test
# git remote set-url origin...
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

git add . --dry-run